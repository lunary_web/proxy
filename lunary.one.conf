# http server block with letsencrypt handling and redirect
server {
    #listen 80; # Only if sysctl net.ipv6.bindv6only = 1
    listen 80;
    #listen [::]:80;

    # Your domain names.
    server_name lunary.one www.lunary.one;

    # Include letsencrypt location
    include letsencrypt.conf;

    # redirect to https version of the site
    return 301 https://lunary.one$request_uri;
}

# https server block which actually proxies to rocket
server {
    #listen 443 ssl http2; # Only if sysctl net.ipv6.bindv6only = 1
    listen 443 ssl;
    #listen [::]:443 ssl http2;

    # Your domain names (same as in the http block)
    server_name lunary.one www.lunary.one;

    # Include letsencrypt location
    include letsencrypt.conf;

    ssl_certificate /etc/letsencrypt/live/lunary.one/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/lunary.one/privkey.pem;
    ssl_session_timeout 5m;
    ssl_protocols TLSv1.2 TLSv1.3; # don't use SSLv3. Ref: POODLE
    ssl_prefer_server_ciphers on;

    location / {
        # Forward requests to rocket
        proxy_pass http://lunary;
    }
}
