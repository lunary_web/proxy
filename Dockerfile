FROM nginx

RUN rm /etc/nginx/conf.d/default.conf
COPY letsencrypt.conf nginx.conf /etc/nginx/
COPY lunary.one.conf /etc/nginx/conf.d/

CMD ["nginx", "-g", "daemon off;"]
